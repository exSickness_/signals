#ifndef SIGNALER_H
	#define SIGNALER_H

#include <stdbool.h>
#include <signal.h>
#include <limits.h>


/*
 * REFERENCES
 * https://www.linuxprogrammingblog.com/code-examples/sigaction
 * https://gist.github.com/aspyct/3462238
*/

void sigHandler(int sig, siginfo_t *siginfo, void *context);
bool checkPrime(int a);
int extractArgs(int argc, char* argv[]);
int primeLoop();
int initSig();


// Global variables to be modified in sigHandler and used in the main loop
int step = 1;
int num = 2;
int skipNext = 0;
long long maxNum = UINT_MAX;

#endif