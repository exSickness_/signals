#include <limits.h> // UINT_MAX
#include <math.h> // sqrt()
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h> // strtoll()
#include <string.h> // memset()
#include <unistd.h>

#include "signaler.h"

/*
 * REFERENCES
 * https://www.linuxprogrammingblog.com/code-examples/sigaction
 * https://gist.github.com/aspyct/3462238
*/


int main(int argc, char* argv[])
{
	/* Main() */

	// initSig function call sets up the signal handling
	if( initSig() == -1) {
		// Error setting up the signal handling
		fprintf(stderr,"Error setting up signal handling.\n");
		return(-1);
	}

	// extractArgs funciton call populates program variables, if any
	if(extractArgs(argc, argv) == -1) {
		// Error parsing command-line arguments
		fprintf(stderr,"Invalid arguments.\n");
		return(-1);
	}

	// primeLoop function call starts the generation of prime numbers in the specified range
	primeLoop();

	return(0);
}


int initSig()
{
	/* Function that initializes the signal handling */

	struct sigaction act;

	memset (&act, '\0', sizeof(act));
	/* Use the sa_sigaction field because the handles has two additional parameters */
	act.sa_sigaction = &sigHandler;

	/* The SA_SIGINFO flag tells sigaction() to use the sa_sigaction field, not sa_handler. */
	act.sa_flags = SA_SIGINFO;

	// Makes sure program can handle following signals
	if(sigaction(SIGHUP, &act, NULL) == -1) {
		perror ("Error: cannot handle SIGHUP");
		return(-1);
	}
	if(sigaction(SIGUSR1, &act, NULL) == -1) {
		perror("Error: cannot handle SIGUSR1"); // Should not happen
		return(-1);
	}
	if(sigaction(SIGUSR2, &act, NULL) == -1) {
		perror("Error: cannot handle SIGUSR2"); // Should not happen
		return(-1);
	}

	return(0);
}


int extractArgs(int argc, char* argv[])
{
	/* Function retrieves command-line argument values */

	int option = -1;
	// Loop to parse out all the command-line arguments
	while ((option = getopt(argc, argv,"r:s:e:")) != -1)
	{
		char* endptr = NULL;

		switch (option)
		{
			case 's': // Start at next prime num after n
				if(num != 2) {
					fprintf(stderr,"Cannot specify -s with -r flag.\n");
					return(-1);
				}
				num = strtoll(optarg,&endptr,10);

				if (endptr == optarg || num < 2) {
					// Nothing parsed from the string.
					fprintf(stderr,"Invalid number.\n");
					return(-1);
				}
				num += 1;
				break;

			case 'r': // Reverse order in which prime numbers are generated
				if(num != 2) {
					fprintf(stderr,"Cannot specify -r with -s flag.\n");
					return(-1);
				}
				num = strtoll(optarg,&endptr,10);

				if (endptr == optarg || num < 2) { // Nothing parsed from the string.
					fprintf(stderr,"Invalid number.\n");
					return(-1);
				}
				step *= -1;
				break;

			case 'e': // Exit program if greater than n
				maxNum = strtoll(optarg,&endptr,10);
				if (endptr == optarg) { // Nothing parsed from the string.
					fprintf(stderr,"Could not extract end number.\n");
					return(-1);
				}
				break;

			 default:
			 	fprintf(stderr,"Invalid flags.\n");
				return(-1);
		}
	}

	return(0);
}


int primeLoop()
{
	/* Main loop that prints out prime numbers in valid range */

	bool isPrime = false;

	// Loop until num is outside valid range.
	while(num < maxNum && num > 1)
	{
		// Loops until a prime number is found
		while(isPrime == false)
		{
			// Checks if the current number is prime
			if( (isPrime = checkPrime(num)) )
			{
				// Decide to skip this prime or not
				if(skipNext) {
					skipNext = 0;
				}
				else {
					printf("%d is prime.\n", num);
				}

				// Reset condition when searching for a prime
				isPrime = false;
				break;
			}
			else { /* Not prime */ }

			// Step the current number.
			num += step;
			// Check to make sure it's in the valid range
			if( num > maxNum || num < 1) {
				return(0);
			}
		}

		// Step the current number after sleeping for a second
		sleep(1);
		num += step;
	}

	return(0);
}


bool checkPrime(int n)
{
	/* Function checks one number for prime */

	if(n == 2) { // 2 is prime.
		return(true);
	}
	else if(n % 2 == 0) { // Automatically not prime if divisible by 2.
		return(false);
	}

	// Calculate the target number to check up to
	int tar = (int)sqrt(n) +1;

	// Loop until target, checking if is a factor of n
	for ( int c = 3 ; c <= tar ; c+=2 ) {
		if ( n %c == 0 ) {
			return(false);
		}
	}
	return(true);
}


void sigHandler(int sig, siginfo_t *siginfo, void *context)
{
	/* Function called upon program recieving a signal */

	// Un-used variables in this function
	(void)context;
	(void)siginfo;

	// Different actions dependent upon signal
	switch(sig)
	{
		case(1):
			printf("Restarting number generation at 2!\n");
			num = 1;
			break;
		case(10):
			printf("Skipping next prime number output!\n");
			skipNext = 1;
			break;
		case(12):
			printf("Reversing order of prime number output!\n");
			step *= -1;
			break;
		default:
			// Unhandled signal
			printf("Unhandled signal!\n");
			break;
	}
}